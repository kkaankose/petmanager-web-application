package com.kkaankose.petmanagerapi.controller;

import com.kkaankose.petmanagerapi.model.request.pet.CreatePetRequest;
import com.kkaankose.petmanagerapi.model.request.pet.UpdatePetRequest;
import com.kkaankose.petmanagerapi.model.request.user.UpdateUserRequest;
import com.kkaankose.petmanagerapi.model.response.pet.PetResponse;
import com.kkaankose.petmanagerapi.model.response.user.UserResponse;
import com.kkaankose.petmanagerapi.service.AuthenticationService;
import com.kkaankose.petmanagerapi.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final AuthenticationService authenticationService;

    public UserController(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @GetMapping
    public List<UserResponse> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserResponse getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PutMapping("/{id}")
    public void updateUser(@Valid @RequestBody UpdateUserRequest updateUserRequest, @PathVariable Long id) {
        userService.updateUser(updateUserRequest, id, authenticationService.getAuthenticatedUserId());
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

}