package com.kkaankose.petmanagerapi.controller;

import com.kkaankose.petmanagerapi.model.request.pet.CreatePetRequest;
import com.kkaankose.petmanagerapi.model.request.pet.UpdatePetRequest;
import com.kkaankose.petmanagerapi.model.response.pet.PetResponse;
import com.kkaankose.petmanagerapi.service.AuthenticationService;
import com.kkaankose.petmanagerapi.service.PetService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {

    private final AuthenticationService authenticationService;
    private final PetService petService;

    public PetController(AuthenticationService authenticationService, PetService petService) {
        this.authenticationService = authenticationService;
        this.petService = petService;
    }

    @GetMapping
    public List<PetResponse> getPets() {
        return petService.getPets(authenticationService.getAuthenticatedUserId());
    }

    @GetMapping("/{id}")
    public PetResponse getPet(@PathVariable Long id) {
        return petService.getPet(id, authenticationService.getAuthenticatedUserId());
    }

    @DeleteMapping("/{id}")
    public void deletePet(@PathVariable Long id) {
        petService.deletePet(id, authenticationService.getAuthenticatedUserId());
    }

    @PostMapping
    public void addPet(@Valid @RequestBody CreatePetRequest createPetRequest) {
        petService.addPet(createPetRequest, authenticationService.getAuthenticatedUserId());
    }

    @PutMapping("/{id}")
    public void updatePet(@Valid @RequestBody UpdatePetRequest updatePetRequest, @PathVariable Long id) {
        petService.updatePet(updatePetRequest, id, authenticationService.getAuthenticatedUserId());
    }
}