package com.kkaankose.petmanagerapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "pets")
@Entity
@Getter
@Setter
public class Pet {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User owner;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "owner_name")
    private String ownerName;

}
