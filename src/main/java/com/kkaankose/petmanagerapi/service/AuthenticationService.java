package com.kkaankose.petmanagerapi.service;

import com.kkaankose.petmanagerapi.entity.User;
import com.kkaankose.petmanagerapi.exception.BusinessException;
import com.kkaankose.petmanagerapi.exception.ErrorCode;
import com.kkaankose.petmanagerapi.model.request.auth.LoginRequest;
import com.kkaankose.petmanagerapi.model.request.auth.RegisterRequest;
import com.kkaankose.petmanagerapi.model.response.LoginResponse;
import com.kkaankose.petmanagerapi.repository.UserRepository;
import com.kkaankose.petmanagerapi.security.JwtService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final JwtService jwtService;

    public AuthenticationService(PasswordEncoder passwordEncoder, UserRepository userRepository, JwtService jwtService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.jwtService = jwtService;
    }

    public void register(RegisterRequest registerRequest) {
        User user = userRepository.findUserByEmail(registerRequest.getEmail());
        if (user != null) {
            throw new RuntimeException("User already exists.");
        }

        User newUser = new User();
        newUser.setName(registerRequest.getName());
        newUser.setEmail(registerRequest.getEmail());
        newUser.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        newUser.setSurname(registerRequest.getSurname());

        userRepository.save(newUser);
    }

    public LoginResponse login(LoginRequest loginRequest) {
        User user = userRepository.findUserByEmail(loginRequest.getEmail());
        if (user == null) {
            throw new BusinessException(ErrorCode.account_missing, "There are no user like that.");
        }

        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
            throw new BusinessException(ErrorCode.validation, "Wrong Password!");
        }

        return LoginResponse.builder()
                .id(user.getId())
                .token(jwtService.createToken(user.getId().toString()))
                .build();
    }

    public Long getAuthenticatedUserId() {
        String principal = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.equals("anonymousUser")) {
            throw new BusinessException(ErrorCode.unauthorized, "Unauthorized user!");
        }
        return Long.parseLong(principal);
    }
}

