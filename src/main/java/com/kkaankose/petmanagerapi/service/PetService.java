package com.kkaankose.petmanagerapi.service;

import com.kkaankose.petmanagerapi.entity.Pet;
import com.kkaankose.petmanagerapi.entity.User;
import com.kkaankose.petmanagerapi.exception.BusinessException;
import com.kkaankose.petmanagerapi.exception.ErrorCode;
import com.kkaankose.petmanagerapi.model.request.pet.CreatePetRequest;
import com.kkaankose.petmanagerapi.model.request.pet.UpdatePetRequest;
import com.kkaankose.petmanagerapi.model.response.pet.PetResponse;
import com.kkaankose.petmanagerapi.repository.PetRepository;
import com.kkaankose.petmanagerapi.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class PetService {

    private final PetRepository petRepository;
    private final UserRepository userRepository;

    public PetService(PetRepository petRepository, UserRepository userRepository) {
        this.petRepository = petRepository;
        this.userRepository = userRepository;
    }

    public List<PetResponse> getPets(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        return user.getPets().stream()
                .map(PetResponse::fromEntity)
                .collect(Collectors.toList());
    }

    public PetResponse getPet(Long id, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        Pet pet = petRepository.findById(id)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no Pet by that ID."));

        return PetResponse.fromEntity(pet);
    }

    public void deletePet(Long id, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        Pet pet = petRepository.findById(id)
                .orElseThrow(() -> new BusinessException(ErrorCode.resource_missing, "There is no Pet by that ID."));

        Set<Pet> pets = user.getPets();
        pets.remove(pet);

        petRepository.delete(pet);
    }

    public void addPet(CreatePetRequest createPetRequest, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        Pet newPet = new Pet();
        newPet.setName(createPetRequest.getName());
        newPet.setType(createPetRequest.getType());
        newPet.setOwner(user);

        Set<Pet> pets = user.getPets();
        pets.add(newPet);
        user.setPets(pets);
        petRepository.save(newPet);
    }

    public void updatePet(UpdatePetRequest updatePetRequest, Long petId, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new BusinessException(ErrorCode.resource_missing, "There is no Guide by that ID."));

        if (userId != pet.getOwner().getId()) {
            throw new BusinessException(ErrorCode.unauthorized, "You cant update this guide.");
        }

        pet.setName(updatePetRequest.getName());

        petRepository.save(pet);
    }
}