package com.kkaankose.petmanagerapi.service;

import com.kkaankose.petmanagerapi.entity.User;
import com.kkaankose.petmanagerapi.exception.BusinessException;
import com.kkaankose.petmanagerapi.exception.ErrorCode;
import com.kkaankose.petmanagerapi.model.request.user.UpdateUserRequest;
import com.kkaankose.petmanagerapi.model.response.user.UserResponse;
import com.kkaankose.petmanagerapi.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<UserResponse> getUsers() {
        List<User> userList = userRepository.findAll();
        return userList.stream()
                .map(UserResponse::fromEntity)
                .collect(Collectors.toList());
    }

    public UserResponse getUser(Long id) {
        User existingUser = userRepository.findById(id)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user by that ID."));

        return UserResponse.fromEntity(existingUser);
    }

    public void updateUser(UpdateUserRequest updateUserRequest, Long userId, Long authenticatedUserId) {
        User existingUser = userRepository.findById(authenticatedUserId)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));

        if (existingUser.getId() != userId) {
            throw new BusinessException(ErrorCode.account_missing, "There is no user like that!");
        }

        existingUser.setName(updateUserRequest.getName());
        existingUser.setSurname(updateUserRequest.getSurname());

        userRepository.save(existingUser);
    }

    public void deleteUser(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new BusinessException(ErrorCode.account_missing, "There is no user like that!"));


        user.setPets(null);
        userRepository.delete(user);
    }
}