package com.kkaankose.petmanagerapi.model.request.auth;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@ToString
public class RegisterRequest {

    @Email(message = "Invalid email")
    @NotEmpty
    private String email;

    @Length(min = 6)
    @NotEmpty
    private String password;

    @NotEmpty
    private String name;

    @NotEmpty
    private String surname;

}
