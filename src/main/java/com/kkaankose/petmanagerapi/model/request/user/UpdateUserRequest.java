package com.kkaankose.petmanagerapi.model.request.user;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UpdateUserRequest {

    @NotNull
    private String name;

    @NotNull
    private String surname;

}