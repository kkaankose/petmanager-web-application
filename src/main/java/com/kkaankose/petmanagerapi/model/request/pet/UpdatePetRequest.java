package com.kkaankose.petmanagerapi.model.request.pet;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UpdatePetRequest {

    @NotNull
    private String name;

}
