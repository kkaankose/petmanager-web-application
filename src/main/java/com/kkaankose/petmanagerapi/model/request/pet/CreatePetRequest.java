package com.kkaankose.petmanagerapi.model.request.pet;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class CreatePetRequest {

    @NotNull
    private String name;

    @NotNull
    private String type;
}