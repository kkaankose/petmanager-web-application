package com.kkaankose.petmanagerapi.model.response.pet;

import com.kkaankose.petmanagerapi.entity.Pet;
import com.kkaankose.petmanagerapi.entity.User;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class PetResponse {

    private Long id;
    private String name;
    private String type;
    private User owner;
    private String ownerName;

    public static PetResponse fromEntity(Pet pet) {
        return PetResponse.builder()
                .id(pet.getId())
                .name(pet.getName())
                .type(pet.getType())
                .ownerName(pet.getOwner().getName())
                .build();
    }
}
