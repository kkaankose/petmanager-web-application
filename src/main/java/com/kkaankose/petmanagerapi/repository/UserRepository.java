package com.kkaankose.petmanagerapi.repository;

import com.kkaankose.petmanagerapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    public User findUserByEmail(String email);

}
