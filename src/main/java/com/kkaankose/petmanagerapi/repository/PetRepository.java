package com.kkaankose.petmanagerapi.repository;

import com.kkaankose.petmanagerapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
